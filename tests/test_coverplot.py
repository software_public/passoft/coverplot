#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `coverplot` package."""

import argparse
import os
import unittest

from unittest.mock import patch
from coverplot.coverplot import get_command_args

MS = ['tests/test.sync']
DL = ['tests/1B.13.AAA.20181231031.dataless']

if os.path.isfile('test.sync.crshd'):
    os.remove('test.sync.crshd')


class TestCoverplot(unittest.TestCase):
    """Tests for `coverplot` package."""

    @patch('argparse.ArgumentParser.parse_args', autospec=True)
    def test_get_command_args(self, mock_parser):
        """Test basic functionality of get_command_args"""

        mock_parser.return_value = argparse.Namespace(ms=[], dl=[])
        res = get_command_args()
        self.assertEqual(len(res[0]), 0,
                         "Error! No mseed file should have been read!")
        self.assertEqual(len(res[1]), 0,
                         "Error! No datalass file should have been read!")

        mock_parser.return_value = argparse.Namespace(ms=MS, dl=[])
        res = get_command_args()
        self.assertEqual(len(res[0]), 1,
                         "No crushed sync written. get_command_args() failed!")
        self.assertEqual(len(res[1]), 0,
                         "Error! No datalass file should have been read!")
        self.assertIn('test.sync.crshd', os.listdir('tests'),
                      "Crushing nonwf channels failed!")

        mock_parser.return_value = argparse.Namespace(ms=MS, dl=DL)
        res = get_command_args()
        self.assertEqual(len(res[1]), 1, "Error! No datalass file read!")
