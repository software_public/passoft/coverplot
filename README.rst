=========
coverplot
=========


* Description: plot time coverage of a dataless and MiniSEED from syncs.

* Usage: coverplot -m mseed_sync_file
         coverplot -m mseed_sync_file -d dataless

* Free software: GNU General Public License v3 (GPLv3)
