# -*- coding: utf-8 -*-

"""Top-level package for coverplot."""

__author__ = """EPIC"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2024.5.0.0'
