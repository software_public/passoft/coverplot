=======
History
=======

2013.295 (2018-06-06)
------------------

* First release on new build system.

2018.206 (2018-07-25)
------------------

* Updated Plot Save, now includes time annotations on saved eps plot file. - dthomas

2018.228 (2018-08-26)
------------------

* Updated Pto be python 2/3 compatible

2020.223 (2020-08-10)
------------------
* Updated to work with Python 3
* Added a unit test to ensure basic functionality of
  coverplot.get_command_args()
* Updated list of platform specific dependencies to be installed when
  pip installing coverplot (see setup.py)
* Installed and tested coverplot against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda package for coverplot that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

2021.176 (2021-06-25)
---------------------
* Update version number that is displayed in GUI

2022.1.0.0 (2022-01-11)
---------------------
* Add new versioning scheme

2022.1.1.0 (2022-05-03)
---------------------
* Remove version constraints on numpy (dependency of matplotlib) following
  latest release of ObsPy which addresses incompatibility issue between ObsPy
  and numpy 1.22.

2023.2.0.0 (2023-07-14)
---------------------
* Remove python 3.7 support

2023.3.0.0 (2023-11-06)
---------------------
* Migrate GUI library from tkinter to PySide2

2024.4.0.0 (2024-01-02)
---------------------
* Migrate GUI library from PySide2 to Pyside6
